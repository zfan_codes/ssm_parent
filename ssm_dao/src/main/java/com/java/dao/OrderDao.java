package com.java.dao;

import com.java.domain.Order;
import com.java.domain.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface OrderDao {
    @Select("select * from orders")
//    @Results 映射多列数据
//    @Result 映射单列数据
    //select 全限类名 + 方法名 == mapperId
    @Results({
            @Result(property = "product",column = "productId",javaType = Product.class,
            one = @One(select = "com.java.dao.ProductDao.findById"))
    })
    List<Order> findAll();

    @Insert("INSERT INTO orders (`orderNum`, `orderTime`, `peopleCount`, `orderDesc`, `payType`, `orderStatus`, `productId`) " +
            "VALUES (#{orderNum}, #{orderTime}, #{peopleCount}, #{orderDesc}, #{payType}, #{orderStatus}, #{product.id})")
    void save(Order order);
}
