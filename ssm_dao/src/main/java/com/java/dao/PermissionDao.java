package com.java.dao;

import com.java.domain.SysPermission;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PermissionDao {
    @Select("select * from sys_permission")
    List<SysPermission> findAll();

    @Select("select * from sys_permission where pid=0")
    List<SysPermission> findFather();

    @Insert("insert into sys_permission (id, permissionName, url, pid) " +
            "value (null, #{permissionName}, #{url}, #{pid});")
    void save(SysPermission permission);
}
