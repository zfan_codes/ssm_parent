package com.java.dao;

import com.java.domain.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ProductDao {

    @Select("select * from product")
    List<Product> findAll();

    // 查询之后获取自增ID
    // keyProperty 主键属性名
    // keyColumn 主键列名
    // before 添加之前true/添加之后false获取主键
    // resultType 主键类型
    // statement 获取主键sql语句
    @Insert("insert into product values(null,#{productNum},#{productName},#{cityName},#{departureTime},#{productPrice},#{productDesc},#{productStatus})")
//    @SelectKey(keyProperty = "id", keyColumn = "id", before = false, resultType = Integer.class, statement = "select last_insert_id()")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    Integer save(Product product);

    @Select("select * from product where id = ${value}")
    Product findById(Integer id);

    @Update("update product set productNum=#{productNum},productName=#{productName},cityName=#{cityName},departureTime=#{departureTime}" +
            ",productPrice=#{productPrice},productDesc=#{productDesc},productStatus=#{productStatus} where id=#{id}")
    void update(Product product);

    @Delete("delete from product where id=#{id}")
    void remove(Integer id);

    @Select("select count(id) as c from product")
    Long findTotalCount();

    @Select("select * from product limit #{param1},#{param2}")
    List<Product> findByPage(Integer rowNum, Integer pageSize);
}
