package com.java.dao;

import com.java.domain.SysRole;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RoleDao {

    @Select("select * from sys_role")
    List<SysRole> findAll();

    @Insert("INSERT INTO sys_role (id,roleName,roleDesc) VALUE(NULL,#{roleName},#{roleDesc})")
    void save(SysRole role);

    @Select("select * from sys_role where roleName=#{roleName}")
    SysRole findByRoleName(@Param("roleName") String roleName);
}
