package com.java.dao;

import com.java.domain.SysUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserDao {
    @Select("select * from sys_user where username=#{username} and status=1")
    SysUser findByUsername(String username);

    @Select("select * from sys_user")
    List<SysUser> findAll();

    @Insert("INSERT INTO sys_user (id,`username`,`email`,`password`,`phoneNum`,`status`) " +
            "VALUE(null,#{username},#{email},#{password},#{phoneNum},#{status})")
    void save(SysUser user);

    @Select("select * from sys_user where username=#{username}")
    SysUser findAllByUsername(String username);

    @Select("select * from sys_user where id = #{id}")
    SysUser findById(@Param("id") Integer userId);
}
