package com.java;

import com.java.dao.ProductDao;
import com.java.domain.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/applicationContext-dao.xml")
public class ProductTest {

    @Autowired
    ProductDao productDao;

    @Test
    public void testFindAll() {
        List<Product> productList = productDao.findAll();
        System.out.println(productList);
    }
}
