package com.java.domain;

public class JsonBody {

    private Integer ret;
    private Object data;
    private String msg;

    public JsonBody() {

    }

    public JsonBody(Integer ret, Object data, String msg) {
        this.ret = ret;
        this.data = data;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getRet() {
        return ret;
    }

    public void setRet(Integer ret) {
        this.ret = ret;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
