package com.java.service;

import com.java.domain.Order;

import java.util.List;

public abstract class OrderService {

    public abstract List<Order> findAll();

    public abstract void save(Order order);
}
