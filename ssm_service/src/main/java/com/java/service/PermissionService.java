package com.java.service;

import com.github.pagehelper.PageInfo;
import com.java.domain.SysPermission;

import java.util.List;

public interface PermissionService {
    PageInfo<SysPermission> findAll(Integer currPage, Integer pageSize);

    List<SysPermission> findFather();

    void save(SysPermission permission);
}
