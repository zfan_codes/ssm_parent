package com.java.service;

import com.github.pagehelper.PageInfo;
import com.java.domain.PageBean;
import com.java.domain.Product;

import java.util.List;

public interface ProductService {

    public List<Product> findAll();

    public Integer save(Product product);

    public Product findById(Integer id);

    public void update(Product product);

    void remove(Integer id);

    void removeMany(Integer[] ids);

    /**
     * 根据分页参数，查询PageBean对象
     *
     * @param currPage 当前页
     * @param pageSize 每页显示数量
     * @return
     */
    PageBean<Product> findByPage(Integer currPage, Integer pageSize);

    PageInfo<Product> findByPageHelper(Integer currPage, Integer pageSize);

    void testFindByPageHelper(Integer currPage,Integer pageSize);
}
