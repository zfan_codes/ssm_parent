package com.java.service;

import com.github.pagehelper.PageInfo;
import com.java.domain.SysRole;

public interface RoleService {

    PageInfo<SysRole> findAllByPage(Integer currPage, Integer pageSize);

    void save(SysRole sysRole);

    Boolean checkUniqueName(String roleName);
}
