package com.java.service;

import com.github.pagehelper.PageInfo;
import com.java.domain.SysUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    PageInfo<SysUser> findAllByPage(Integer currPage, Integer pageSize);

    void save(SysUser user);

    Boolean isUniqueUsername(String username);

    SysUser findDetails(Integer userId);
}
