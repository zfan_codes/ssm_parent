package com.java.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.java.dao.PermissionDao;
import com.java.domain.SysPermission;
import com.java.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    PermissionDao permissionDao;

    @Override
    public PageInfo<SysPermission> findAll(Integer currPage, Integer pageSize) {
        PageHelper.startPage(currPage, pageSize);
        List<SysPermission> list = permissionDao.findAll();
        return new PageInfo<>(list);
    }

    @Override
    public List<SysPermission> findFather() {
        return permissionDao.findFather();
    }

    @Override
    public void save(SysPermission permission) {
        permissionDao.save(permission);
    }


}
