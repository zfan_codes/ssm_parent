package com.java.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.java.dao.ProductDao;
import com.java.domain.PageBean;
import com.java.domain.Product;
import com.java.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDao productDao;

    @Override
    public List<Product> findAll() {
        return productDao.findAll();
    }

    @Override
    public Integer save(Product product) {
        return productDao.save(product);
    }

    @Override
    public Product findById(Integer id) {
        return productDao.findById(id);
    }

    @Override
    public void update(Product product) {
        productDao.update(product);
    }

    @Override
    public void remove(Integer id) {
        productDao.remove(id);
    }

    @Override
    public void removeMany(Integer[] ids) {
        // 判断IDS是否为空
        if (ids == null) {
            return;
        }
        for (Integer id : ids) {
            productDao.remove(id);
        }
    }

    @Override
    public PageBean<Product> findByPage(Integer currPage, Integer pageSize) {
        Long totalCount = productDao.findTotalCount();
        List<Product> productList = productDao.findByPage((currPage - 1) * pageSize, pageSize);

        PageBean<Product> pageBean = new PageBean<>();
        pageBean.setCurrPage(currPage);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalCount(totalCount);
        pageBean.setTotalPage((int) Math.ceil(totalCount * 1.0 / pageSize));
        pageBean.setList(productList);
        return pageBean;
    }

    @Override
    public PageInfo<Product> findByPageHelper(Integer currPage, Integer pageSize) {
        PageHelper.startPage(currPage, pageSize);
        List<Product> productList = productDao.findAll();
        return new PageInfo<>(productList, 3);
    }

    @Override
    public void testFindByPageHelper(Integer currPage, Integer pageSize) {
        // 为分页初始化参数
        PageHelper.startPage(currPage, pageSize);
        // 查询全部
        List<Product> productList = productDao.findAll();
        // 创建一个PageInfo对象 -- 相当于自定义PageBean，需要通过构造传入查询的集合对象
        // 页面最多显示3个页码
        PageInfo<Product> pageInfo = new PageInfo<>(productList, 3);
        System.out.println(pageInfo);
        System.out.println("当前页:" + pageInfo.getPageNum());
        System.out.println("每页条数:" + pageInfo.getPageSize());
        System.out.println("总页数:" + pageInfo.getPages());
        System.out.println("总条数:" + pageInfo.getTotal());
        System.out.println("数据:" + pageInfo.getList().size());
        System.out.println("上一页页码:" + pageInfo.getPrePage());
        System.out.println("下一页页码:" + pageInfo.getNextPage());
        System.out.println("是否第一页:" + pageInfo.isIsFirstPage());
        System.out.println("是否最后一页:" + pageInfo.isIsLastPage());
        System.out.println("页面显示第一个页码:" + pageInfo.getNavigateFirstPage());
        System.out.println("页面显示最后一个页码:" + pageInfo.getNavigateLastPage());
    }
}
