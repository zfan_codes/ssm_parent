package com.java.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.java.dao.RoleDao;
import com.java.domain.SysRole;
import com.java.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleDao roleDao;

    @Override
    public PageInfo<SysRole> findAllByPage(Integer currPage, Integer pageSize) {
        PageHelper.startPage(currPage, pageSize);
        List<SysRole> list = roleDao.findAll();
        return new PageInfo<>(list);
    }

    @Override
    public void save(SysRole role) {
        roleDao.save(role);
    }

    @Override
    public Boolean checkUniqueName(String roleName) {
        SysRole role = roleDao.findByRoleName(roleName);
        return role==null;
    }
}
