package com.java;

import com.java.domain.Product;
import com.java.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:spring/*.xml")
public class ProductServiceTest {

    @Autowired
    ProductService productService;

    @Test
    public void testFindAll() {
        List<Product> productList = productService.findAll();
        System.out.println(productList);
    }
}
