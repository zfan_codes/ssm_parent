package com.java.service;

import com.java.domain.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:spring/*.xml")
public class ProductServiceTest {
    @Autowired
    ProductService productService;

    @Test
    public void testFindById() {
        Product product = productService.findById(1);
        System.out.println(product);
    }

    @Test
    public void testFindByPageHelper() {
        productService.testFindByPageHelper(1, 2);
    }
}