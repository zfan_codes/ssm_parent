package com.java.service.impl;

import com.github.pagehelper.PageInfo;
import com.java.domain.SysRole;
import com.java.service.RoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath*:spring/*.xml")
public class RoleServiceImplTest {

    @Autowired
    RoleService roleService;

    @Test
    public void findAllByPage() {
        PageInfo<SysRole> page = roleService.findAllByPage(1, 2);
        System.out.println(page);
    }
}