package com.java.controller;

import com.java.domain.Order;
import com.java.domain.Product;
import com.java.service.OrderService;
import com.java.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;
    @Autowired
    ProductService productService;

    @RequestMapping("/findAll")
    ModelAndView findAll() {
        List<Order> orderList = orderService.findAll();
        ModelAndView view = new ModelAndView("order-list");
        view.addObject("orderList", orderList);
        return view;
    }

    @RequestMapping("/add")
    ModelAndView add() {
        List<Product> productList = productService.findAll();
        ModelAndView view = new ModelAndView("order-add");
        view.addObject("productList", productList);
        return view;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    String save(Order order) {
        orderService.save(order);
        return "redirect:/order/findAll";
    }
}
