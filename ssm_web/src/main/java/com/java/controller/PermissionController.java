package com.java.controller;

import com.github.pagehelper.PageInfo;
import com.java.domain.SysPermission;
import com.java.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("permission")
public class PermissionController {

    @Autowired
    PermissionService permissionService;

    @RequestMapping("findAll")
    ModelAndView findAll(
            @RequestParam(value = "currPage", required = false, defaultValue = "1") Integer currPage,
            @RequestParam(value = "pageSize", defaultValue = "5", required = false) Integer pageSize
    ) {
        PageInfo<SysPermission> pageInfo = permissionService.findAll(currPage, pageSize);
        ModelAndView view = new ModelAndView("permission-list");
        view.addObject("pageInfo", pageInfo);
        return view;
    }

    @RequestMapping("save")
    ModelAndView save() {
        List<SysPermission> list = permissionService.findFather();
        ModelAndView view = new ModelAndView("permission-add");
        view.addObject("list", list);
        return view;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    String save(SysPermission permission) {
        permissionService.save(permission);
        return "redirect:/permission/findAll";
    }
}
