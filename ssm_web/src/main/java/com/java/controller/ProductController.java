package com.java.controller;

import com.github.pagehelper.PageInfo;
import com.java.domain.PageBean;
import com.java.domain.Product;
import com.java.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;

    /**
     * 分页助手查询
     * @param currPage
     * @param pageSize
     * @return
     */
    @RequestMapping("/findAll")
    public ModelAndView findAll(
            @RequestParam(value = "currPage", required = false, defaultValue = "1") Integer currPage,
            @RequestParam(value = "pageSize", required = false, defaultValue = "2") Integer pageSize) {

        // 准备数据：分页数据
        PageInfo<Product> pageInfo = productService.findByPageHelper(currPage, pageSize);
        ModelAndView view = new ModelAndView("product-list-page-helper");
        view.addObject("pageInfo", pageInfo);
        return view;
    }

    /**
     * 手动分页查询
     * RequestParam 请求参数绑定
     * ----- name：别名value，指定页面参数名称
     * ----- required: 是否必要参数
     * ----- defaultValue： 默认值
     *
     * @return
     */
    @RequestMapping("/findAll2")
    public ModelAndView findAll2(
            @RequestParam(value = "currPage", required = false, defaultValue = "1") Integer currPage,
            @RequestParam(value = "pageSize", required = false, defaultValue = "2") Integer pageSize) {

        // 准备数据：分页数据
        PageBean<Product> pageBean = productService.findByPage(currPage, pageSize);
        ModelAndView view = new ModelAndView("product-list-page");
        view.addObject("pageBean", pageBean);
        return view;
    }

    @RequestMapping("/findAll1")
    public ModelAndView findAll1() {
        List<Product> productList = productService.findAll();

        ModelAndView view = new ModelAndView("product-list");
        view.addObject("productList", productList);
        return view;
    }



    @RequestMapping("/save")
    public String save() {
        return "product-add";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(Product product) {
        Integer id = productService.save(product);
        System.out.println(id);
        return "redirect:/product/findAll";
    }

    @RequestMapping("/update")
    public ModelAndView update(Integer id) {
        Product product = productService.findById(id);

        ModelAndView view = new ModelAndView("product-update");
        view.addObject("product", product);
        return view;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(Product product) {
        productService.update(product);
        return "redirect:/product/update?id=" + product.getId();
    }

    @RequestMapping("/remove")
    public String remove(Integer id) {
        productService.remove(id);
        return "redirect:/product/findAll";
    }

    /**
     * 删除多个产品
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/removeMany", method = RequestMethod.POST)
    public String removeMany(Integer[] ids) {
        productService.removeMany(ids);
        return "redirect:/product/findAll";
    }
}
