package com.java.controller;

import com.github.pagehelper.PageInfo;
import com.java.domain.SysRole;
import com.java.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("role")
public class RoleController {
    @Autowired
    RoleService roleService;

    @RequestMapping("findAll")
    ModelAndView findAll(
            @RequestParam(value = "currPage", defaultValue = "1", required = false) Integer currPage,
            @RequestParam(value = "pageSize", defaultValue = "5", required = false) Integer pageSize
    ) {

        PageInfo<SysRole> pageInfo = roleService.findAllByPage(currPage, pageSize);
        ModelAndView view = new ModelAndView("role-list");
        view.addObject("pageInfo", pageInfo);
        return view;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    String save(SysRole role) {
        roleService.save(role);
        return "redirect:/role/findAll";
    }

    @ResponseBody
    @RequestMapping(value = "checkUniqueName", method = RequestMethod.POST)
    String checkUniqueName(String roleName) {
        Boolean bool = roleService.checkUniqueName(roleName);
        return bool.toString();
    }
}
