<%-- 2019/7/110:46 --%>
<%@ page isELIgnored="false" contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="box-footer">
    <div class="pull-left">
        <div class="form-group form-inline">
            总共${pageInfo.pages} 页，共${pageInfo.total} 条数据。 每页
            <select id="pageSize" onchange="gotoPage(1)" class="form-control">
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="5">5</option>
                <option value="10">10</option>
            </select> 条
        </div>
    </div>

    <div class="box-tools pull-right">
        <ul class="pagination">
            <c:if test="${pageInfo.navigateFirstPage != pageInfo.navigateLastPage}">
                <%--在超链接中访问js函数，必须加前缀：javascript--%>
                <li><a href="javascript:gotoPage(1)" aria-label="Previous">首页</a></li>
                <li><a href="javascript:gotoPage(${pageInfo.prePage})">上一页</a></li>

                <%--begin:从哪儿开始--%>
                <%--end:到哪儿结束--%>

                <c:forEach begin="${pageInfo.navigateFirstPage}" end="${pageInfo.navigateLastPage}" var="i">
                    <c:choose>
                        <c:when test="${pageInfo.pageNum==i}">
                            <li class="active"><a href="javascript:void(0);">${i}</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="javascript:gotoPage(${i})">${i}</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>

                <li><a href="javascript:gotoPage(${pageInfo.nextPage})">下一页</a></li>
                <li><a href="javascript:gotoPage(${pageInfo.pages})" aria-label="Next">尾页</a></li>
            </c:if>
        </ul>
    </div>

</div>

<script>
    $("#pageSize option[value=${pageInfo.pageSize}]").prop("selected", "selected");

    function gotoPage(currPage) {
        //获取每页显示的条数
        var pageSize = $("#pageSize").val();
        if (currPage < 1) {
            return;
        }
        if (currPage > ${pageInfo.pages}) {
            return;
        }
        location.href = "${pageContext.request.contextPath}${gotoUrl}?currPage=" + currPage + "&pageSize=" + pageSize;
    }
</script>