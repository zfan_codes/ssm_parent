<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>

<rapid:override name="content">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            权限资源管理
            <small>权限资源表单</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a href="${pageContext.request.contextPath}/permission/findAll.do">权限资源管理</a></li>
            <li class="active">权限资源表单</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <form action="${pageContext.request.contextPath}/permission/save"
          method="post">
        <!-- 正文区域 -->
        <section class="content"> <!--产品信息-->

            <div class="panel panel-default">
                <div class="panel-heading">权限资源信息</div>
                <div class="row data-type">

                    <div class="col-md-2 title">权限资源名称</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="permissionName"
                               placeholder="权限资源名称" value="">
                    </div>
                    <div class="col-md-2 title">权限资源URL</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="url"
                               placeholder="url" value="">
                    </div>
                    <div class="col-md-2 title">父级</div>
                    <div class="col-md-4 data">
                        <select name="pid" value="" class="form-control select2 select2-hidden-accessible" style="width: 100px">
                            <option value="0">父级菜单</option>
                            <c:forEach items="${list}" var="item">
                                <option value="{$item}">${item.permissionName}</option>
                            </c:forEach>

                        </select>
                    </div>

                </div>
            </div>
            <!--订单信息/--> <!--工具栏-->
            <div class="box-tools text-center">
                <button type="submit" class="btn bg-maroon">保存</button>
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/--> </section>
        <!-- 正文区域 /-->
    </form>
</rapid:override>

<rapid:override name="js">
    <script>
        setSidebarActive("permission-findAll");
    </script>
</rapid:override>

<jsp:include page="base.jsp" flush="true"/>