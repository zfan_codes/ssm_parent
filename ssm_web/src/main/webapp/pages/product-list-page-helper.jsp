<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>

<rapid:override name="content">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            产品管理
            <small>全部产品</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a
                    href="${pageContext.request.contextPath}/pages/product-list.jsp">产品管理</a></li>

            <li class="active">全部产品</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>

            <div class="box-body">

                <!-- 数据表格 -->
                <div class="table-box">

                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick='location.href="${pageContext.request.contextPath}/pages/product-add.jsp"'>
                                    <i class="fa fa-file-o"></i> 新建
                                </button>
                                <button type="button" class="btn btn-default" title="删除"
                                        onclick='delMany()'>
                                    <i class="fa fa-trash-o"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="开启"
                                        onclick='confirm("你确认要开启吗？")'>
                                    <i class="fa fa-check"></i> 开启
                                </button>
                                <button type="button" class="btn btn-default" title="屏蔽"
                                        onclick='confirm("你确认要屏蔽吗？")'>
                                    <i class="fa fa-ban"></i> 屏蔽
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick="window.location.reload();">
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm"
                                   placeholder="搜索"> <span
                                class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!--工具栏/-->
                        <%--如果发现表单提交的数据为null，则把表单移动到表格之外--%>
                    <form id="delForm" action="${pageContext.request.contextPath}/product/delMany">
                        <!--数据列表-->
                        <table id="dataList"
                               class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="" style="padding-right: 0px;"><input
                                        id="selall" type="checkbox" class="icheckbox_square-blue">
                                </th>
                                <th class="sorting_asc">ID</th>

                                <th class="sorting">产品编号</th>
                                <th class="sorting">产品名称</th>
                                <th class="sorting">出发城市</th>
                                <th class="sorting">出发日期</th>
                                <th class="sorting">产品价格</th>
                                <th class="sorting">产品状态</th>

                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                                <%--
                                    foreache循环标签
                                    items: 要循环的集合对象
                                    var：循环中的每一个对象
                                --%>
                            <c:forEach items="${pageInfo.list}" var="product">
                                <tr>
                                    <td><input name="ids" type="checkbox" value="${product.id}"></td>
                                    <td>${product.id}</td>

                                    <td>${product.productNum}</td>
                                    <td>${product.productName}</td>
                                    <td>${product.cityName}</td>
                                    <td>
                                            <%--日期格式化库中的标签
                                                value： 具体的日期格式
                                                pattern: 转换要使用的格式
                                            --%>
                                        <fmt:formatDate value="${product.departureTime}"
                                                        pattern="yyyy-MM-dd HH:mm"></fmt:formatDate>
                                    </td>
                                    <td>${product.productPrice}</td>
                                    <td>${product.productStatus == 1?"开启":"关闭"}</td>

                                    <td class="text-center">
                                        <button type="button" class="btn bg-olive btn-xs"
                                                onclick='delOne(${product.id})'>删除
                                        </button>
                                        <button type="button" class="btn bg-olive btn-xs"
                                                onclick='location.href="all-order-manage-edit.html"'>订单
                                        </button>
                                        <button type="button" class="btn bg-olive btn-xs"
                                                onclick='location.href="${pageContext.request.contextPath}/product/updateUI?id=${product.id}"'>
                                            修改
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>

                        </table>
                        <!--数据列表/-->
                    </form>
                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick='location.href="all-order-manage-edit.html"'>
                                    <i class="fa fa-file-o"></i> 新建
                                </button>
                                <button type="button" class="btn btn-default" title="删除"
                                        onclick='confirm("你确认要删除吗？")'>
                                    <i class="fa fa-trash-o"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="开启"
                                        onclick='confirm("你确认要开启吗？")'>
                                    <i class="fa fa-check"></i> 开启
                                </button>
                                <button type="button" class="btn btn-default" title="屏蔽"
                                        onclick='confirm("你确认要屏蔽吗？")'>
                                    <i class="fa fa-ban"></i> 屏蔽
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick="window.location.reload();">
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm"
                                   placeholder="搜索"> <span
                                class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!--工具栏/-->


                </div>
                <!-- 数据表格 /-->

            </div>
            <!-- /.box-body -->

            <jsp:include page="_pageInfo.jsp">
                <jsp:param name="pageInfo" value="${pageInfo}"/>
                <jsp:param name="gotoUrl" value="/product/findAll"/>
            </jsp:include>

        </div>

    </section>
    <!-- 正文区域 /-->
</rapid:override>

<rapid:override name="js">
    <script>
        function delOne(id) {
            if (confirm("是否确定删除？")) {
                location.href = "${pageContext.request.contextPath}/product/remove?id=" + id;
            }
        }

        function delMany() {
            if (confirm("是否确定删除？")) {
                var form = $("#delManyForm")
                form.submit();
            }
        }

        setSidebarActive("product");
    </script>
</rapid:override>

<jsp:include page="base.jsp" flush="true"/>