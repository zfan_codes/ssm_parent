<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<rapid:override name="content">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            用户管理
            <small>全部用户</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a
                    href="${pageContext.request.contextPath}/user/findAll.do">用户管理</a></li>

            <li class="active">全部用户</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>

            <div class="box-body">

                <!-- 数据表格 -->
                <div class="table-box">

                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick="location.href='${pageContext.request.contextPath}/user/save'">
                                    <i class="fa fa-file-o"></i> 新建
                                </button>

                                <button type="button" class="btn btn-default" title="刷新">
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm"
                                   placeholder="搜索"> <span
                                class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <table id="dataList"
                           class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th class="" style="padding-right: 0px"><input
                                    id="selall" type="checkbox" class="icheckbox_square-blue">
                            </th>
                            <th class="sorting_asc">ID</th>
                            <th class="sorting_desc">用户名</th>
                            <th class="sorting_asc sorting_asc_disabled">邮箱</th>
                            <th class="sorting_desc sorting_desc_disabled">联系电话</th>
                            <th class="sorting">状态</th>
                            <th class="text-center">操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${pageInfo.list}" var="user">
                            <tr>
                                <td><input name="ids" type="checkbox"></td>
                                <td>${user.id }</td>
                                <td>${user.username }</td>
                                <td>${user.email }</td>
                                <td>${user.phoneNum }</td>
                                <td>${user.status==1?"正常":"关闭"}</td>
                                <td class="text-center">
                                    <a href="${pageContext.request.contextPath}/pages/user-show.jsp"
                                       class="btn bg-olive btn-xs">详情</a>
                                    <a href="${pageContext.request.contextPath}/pages/user-role-add.jsp"
                                       class="btn bg-olive btn-xs">添加角色</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <!--数据列表/-->

                </div>
                <!-- 数据表格 /-->

            </div>
            <!-- /.box-body -->

            <!-- .box-footer-->
            <jsp:include page="_pageInfo.jsp">
                <jsp:param name="pageInfo" value="${pageInfo}"/>
                <jsp:param name="gotoUrl" value="/user/findAll"/>
            </jsp:include>
            <!-- /.box-footer-->

        </div>

    </section>
    <!-- 正文区域 /-->
</rapid:override>

<rapid:override name="js">
    <script>
        setSidebarActive("user-findAll");

    </script>
</rapid:override>

<jsp:include page="base.jsp" flush="true"/>
